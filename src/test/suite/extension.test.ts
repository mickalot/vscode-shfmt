import { strict as assert } from 'assert'
import fs from 'fs/promises'
import path from 'path'
import sinon from 'sinon'
import vscode from 'vscode'
import { workspaceRoot } from '.'

async function assertFormats(document: string | vscode.TextDocument, expectedFile: string) {
	if (typeof document === 'string') {
		document = await open(document)
	}
	await format(document)
	assert.equal(document.getText(), await read(expectedFile))
}

async function read(filename: string): Promise<string> {
	const file = path.join(workspaceRoot, filename)
	return fs.readFile(file, { encoding: 'utf8' })
}

async function open(filename: string): Promise<vscode.TextDocument> {
	const file = path.join(workspaceRoot, filename)
	return vscode.workspace.openTextDocument(vscode.Uri.file(file))
}

async function untitled(filename: string): Promise<vscode.TextDocument> {
	const file = path.join(workspaceRoot, filename)
	const content = await fs.readFile(file, { encoding: 'utf8' })
	const language = 'shellscript'
	return vscode.workspace.openTextDocument({ language, content })
}

async function format(document: vscode.TextDocument) {
	await vscode.window.showTextDocument(document)
	await vscode.commands.executeCommand('editor.action.formatDocument')
}

function config(section: string): vscode.WorkspaceConfiguration {
	const uri = vscode.workspace.workspaceFolders?.[0].uri
	const languageId = 'shellscript'
	return vscode.workspace.getConfiguration(section, { uri, languageId })
}

describe('the extension', () => {
	before(async () => {
		await vscode.extensions.getExtension('mkhl.shfmt')?.activate()
	})

	beforeEach(async () => {
		await vscode.commands.executeCommand('workbench.action.files.revert')
		await vscode.commands.executeCommand('workbench.action.closeAllEditors')
	})

	describe('with tab indentation', () => {
		before(async () => {
			await config('editor').update('detectIndentation', false, undefined, true)
			await config('editor').update('insertSpaces', false, undefined, true)
		})
		after(async () => {
			await config('editor').update('detectIndentation', undefined)
			await config('editor').update('insertSpaces', undefined)
		})

		it('formats defaults.sh', async () => {
			await assertFormats('defaults.sh', 'canonical.sh')
		})
	})

	describe('with 2 space indentation', () => {
		before(async () => {
			await config('editor').update('detectIndentation', false, undefined, true)
			await config('editor').update('insertSpaces', true, undefined, true)
			await config('editor').update('tabSize', 2, undefined, true)
		})
		after(async () => {
			await config('editor').update('detectIndentation', undefined)
			await config('editor').update('insertSpaces', undefined)
			await config('editor').update('tabSize', undefined)
		})

		it('formats untitled', async () => {
			const document = await untitled('defaults.sh')
			await assertFormats(document, 'canonical-2space.sh')
		})
	})

	describe('with detected indentation', () => {
		before(async () => {
			await config('editor').update('detectIndentation', true, undefined, true)
			await config('editor').update('insertSpaces', true, undefined, true)
		})
		after(async () => {
			await config('editor').update('detectIndentation', undefined)
			await config('editor').update('insertSpaces', undefined)
		})

		it('formats untitled', async () => {
			const document = await untitled('defaults.sh')
			await assertFormats(document, 'canonical.sh')
		})
	})

	describe('with the `cat` formatter', () => {
		before(async () => {
			await config('shfmt').update('executablePath', path.join(workspaceRoot, 'shcat'))
		})
		after(async () => {
			await config('shfmt').update('executablePath', undefined)
		})

		it('leaves defaults.sh unchanged', async () => {
			await assertFormats('defaults.sh', 'defaults.sh')
		})
	})

	describe('with editorconfig options', () => {
		before(async () => {
			await config('shfmt').update('formatIgnored', false)
		})
		after(async () => {
			await config('shfmt').update('formatIgnored', undefined)
		})

		it('leaves ignored.sh unchanged', async () => {
			await assertFormats('ignored.sh', 'ignored.sh')
		})

		it('formats options.sh', async () => {
			await assertFormats('options.sh', 'canonical-options.sh')
		})
	})

	describe('when shfmt fails', () => {
		it('leaves invalid.sh unchanged', async () => {
			sinon.stub(vscode.window, 'showErrorMessage').resolves(undefined)
			const document = await open('invalid.sh')
			await assertFormats(document, 'invalid.sh')
			const diagnostics = vscode.languages.getDiagnostics(document.uri)
			const position = new vscode.Position(2, 1)
			const range = new vscode.Range(position, position)
			const expected = new vscode.Diagnostic(range, '"done" can only be used to end a loop')
			assert.deepEqual(diagnostics, [expected])
		})
	})
})
