import _cp from 'child_process'
import _glob from 'glob'
import Mocha from 'mocha'
import path from 'path'
import sinon from 'sinon'
import util from 'util'
import vscode from 'vscode'

const execFile = util.promisify(_cp.execFile)
const glob = util.promisify(_glob)

export const workspaceRoot = path.resolve(__dirname, '../../../test/workspace')

async function requireShfmt(): Promise<void> {
	await execFile('shfmt', ['-version'])
}

export async function run(): Promise<void> {
	const mocha = new Mocha({
		ui: 'bdd',
		color: true,
		rootHooks: {
			async beforeAll() {
				await requireShfmt()
			},
			afterEach() {
				sinon.restore()
			},
			async afterAll() {
				await vscode.commands.executeCommand('workbench.action.closeAllEditors')
				await vscode.commands.executeCommand('workbench.action.closeFolder')
			},
		},
	})

	const testsRoot = path.resolve(__dirname, '..')

	const files = await glob('**/**.test.js', { cwd: testsRoot })
	files.forEach((f) => mocha.addFile(path.resolve(testsRoot, f)))

	// mocha has a runAsync that returns a promise but @types don't know about it https://github.com/DefinitelyTyped/DefinitelyTyped/discussions/59250
	const failures = await new Promise<number>((c) => mocha.run(c))
	if (failures > 0) {
		const err = `${failures} tests failed.`
		console.error(err)
		throw new Error(err)
	}
}
