import path from 'path'
import cp from 'child_process'

import { runTests, downloadAndUnzipVSCode, resolveCliArgsFromVSCodeExecutablePath } from '@vscode/test-electron'

async function main() {
	try {
		const vscodeExecutablePath = await downloadAndUnzipVSCode()
		const [cli, ...args] = resolveCliArgsFromVSCodeExecutablePath(vscodeExecutablePath)
		cp.spawnSync(cli, [...args, `--install-extension=editorconfig.editorconfig`], { encoding: 'utf8', stdio: 'inherit' })
		const extensionDevelopmentPath = path.resolve(__dirname, '../../')
		const extensionTestsPath = path.resolve(__dirname, './suite/index')
		const workspacePath = path.resolve(__dirname, '../../test/workspace')
		const launchArgs = [workspacePath, '--install-extension', 'editorconfig.editorconfig']
		await runTests({ vscodeExecutablePath, extensionDevelopmentPath, extensionTestsPath, launchArgs })
	} catch (err) {
		console.error('Failed to run tests')
		process.exit(1)
	}
}

void main()
