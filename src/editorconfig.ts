import vscode from 'vscode'

export type EditorConfig = Record<string, string | number | boolean>

type Api =
	| undefined
	| { resolveCoreConfig(doc: vscode.TextDocument): Promise<EditorConfig> }

const extensionId = 'editorconfig.editorconfig'

function extension(): Api {
	return vscode.extensions.getExtension<Api>(extensionId)?.exports
}

export async function resolve(document: vscode.TextDocument): Promise<EditorConfig> {
	const resolved = await extension()?.resolveCoreConfig(document)
	return resolved ?? {}
}
